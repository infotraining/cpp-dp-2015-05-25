#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include <memory>
#include "shape.hpp"
#include "clone_factory.hpp"


namespace Drawing
{

    using ShapePtr = std::shared_ptr<Shape>;

    // TO DO: zaimplementowac kompozyt grupujący kształty geometryczne
    class ShapeGroup : public Shape
    {
        std::vector<ShapePtr> shapes_;

    public:
        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& s : source.shapes_)
                shapes_.emplace_back(s->clone());
        }

        ShapeGroup& operator=(const ShapeGroup& source)
        {
            ShapeGroup temp(source);
            swap(temp);

            return *this;
        }

        ShapeGroup(ShapeGroup&& sg) = default;
        ShapeGroup& operator=(ShapeGroup&&) = default;


        void swap(ShapeGroup& other)
        {
            shapes_.swap(other.shapes_);
        }

        void draw() const
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        void move(int dx, int dy)
        {
            for(const auto& s : shapes_)
                s->move(dx, dy);
        }

        void read(std::istream& in)
        {
            int count;
            in >> count;

            for(int i = 0; i < count; ++i)
            {
                std::string type_identifier;
                in >> type_identifier;

                auto& shape_factory = get_factory();
                auto shape = ShapePtr(shape_factory.create(type_identifier));
                shape->read(in);

                add(shape);
            }
        }

        void write(std::ostream& out)
        {
            out << "ShapeGroup " << shapes_.size() << std::endl;

            for(const auto& s : shapes_)
                s->write(out);
        }

        Shape* clone() const
        {
            return new ShapeGroup(*this);
        }

        void add(ShapePtr shape)
        {
            shapes_.emplace_back(shape);
        }

    protected:
        virtual Factory& get_factory() const
        {
            return ShapeFactory::instance();
        }
    };
}

#endif /*SHAPE_GROUP_HPP_*/
