#include <memory>
#include "decorator.hpp"

using namespace std;

void client(Component& c)
{
    c.operation();
}

int main()
{
	 // Create ConcreteComponent and two Decorators
     shared_ptr<Decorator> d2 = make_shared<ConcreteDecoratorB>(
                                    make_shared<ConcreteDecoratorA>(
                                        make_shared<ConcreteComponent>()));

     client(*d2);

     cout << endl << endl;
}
