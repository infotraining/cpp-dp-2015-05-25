#ifndef SHAPE_FACTORY_HPP_
#define SHAPE_FACTORY_HPP_

#include "singleton_holder.hpp"
#include "factory.hpp"
#include "shape.hpp"
#include <string>

namespace Drawing
{
	typedef GenericFactory::Factory<Shape, std::string> ShapeFactoryType;
	typedef GenericSingleton::SingletonHolder<ShapeFactoryType> ShapeFactory;
}

#endif /* SHAPE_FACTORY_HPP_ */
