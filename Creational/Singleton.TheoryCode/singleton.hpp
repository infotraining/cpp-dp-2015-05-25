#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>

namespace MutexSingleton
{
    class Singleton
    {
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }

        static Singleton& instance()
        {
            if (!instance_)
            {
                std::lock_guard<std::mutex> lk{mtx_};

                if (!instance_)
                {
                    Singleton* temp = new Singleton();
                    instance_ = temp;

//                    // 1
//                    void* raw_mem = ::operator new(sizeof(Singleton));

//                    // 2
//                    new (raw_mem) Singleton;

//                    // 3
//                    instance_.store(static_cast<Singleton*>(raw_mem));
                }
            }

            return *instance_;
        }

    private:
        static std::atomic<Singleton*> instance_;
        static std::mutex mtx_;

        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }
    };


    std::atomic<Singleton*> Singleton::instance_ {};
    std::mutex Singleton::mtx_;
}

namespace MeyersSingleton
{
    class Singleton
    {
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }
    public:
        static Singleton& instance()
        {
            static Singleton unique_instance;

            return unique_instance;
        }

        void do_something()
        {
            std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
        }

    private:
        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }
    };
}

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

    ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
    {
        std::cout << "Singleton has been destroyed!" << std::endl;
    }

	static Singleton& instance()
	{
        std::call_once(init_flag_, [] { instance_.reset(new Singleton()); });

		return *instance_;
	}

	void do_something();

private:
    static std::unique_ptr<Singleton> instance_;  // uniqueInstance
    static std::once_flag init_flag_;
	
	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 


};

std::unique_ptr<Singleton> Singleton::instance_ {nullptr};
std::once_flag Singleton::init_flag_;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
