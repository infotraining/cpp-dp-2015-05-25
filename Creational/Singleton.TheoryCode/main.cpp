#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    MeyersSingleton::Singleton::instance().do_something();

    auto& singleObject = MeyersSingleton::Singleton::instance();
	singleObject.do_something();
}
