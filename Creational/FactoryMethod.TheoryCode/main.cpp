#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <deque>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{
        unique_ptr<Service> service = creator_->create_service();

        string result = service->run();
        cout << "Client is using: " << result << endl;
	}
};

int main()
{
    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("SrvA", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("SrvB", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("SrvC", make_shared<ConcreteCreatorC>()));

    Client client(creators["SrvC"]);
	client.use();

    // iterator
    list<std::string> words = { "one", "two", "three" };

//    for(const auto& item : words)
//        std::cout << item << " ";

    for(auto it = words.begin(); it != words.end(); ++it)
    {
        const auto& item = *it;
        std::cout << item << " ";
    }

    std::cout << std::endl;
}











