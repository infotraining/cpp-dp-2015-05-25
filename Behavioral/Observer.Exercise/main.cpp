#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestor�w zainteresowanych powiadomieniami o zmianach kursu sp�ek
	// TODO:
    Investor kulczyk("Kulczyk");
    Investor solorz("Solorz");

    misys.connect([&kulczyk](const string& symbol, double price) { kulczyk.update_portfolio(symbol, price);});
    tpsa.connect([&kulczyk](const string& symbol, double price) { kulczyk.update_portfolio(symbol, price);});
    auto conn_ibm_solorz = ibm.connect([&solorz](const string& symbol, double price) { solorz.update_portfolio(symbol, price);});

	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

    cout << "\n\n";

    conn_ibm_solorz.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
