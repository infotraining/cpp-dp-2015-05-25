#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <set>
#include <boost/signals2.hpp>

//class Observer
//{
//public:
//    virtual void update(const std::string& symbol, double price) = 0;
//    virtual ~Observer()
//    {
//    }
//};

using PriceChangedSignal = boost::signals2::signal<void (const std::string& symbol, double price)>;
using PriceChangedSlot = PriceChangedSignal::slot_type;

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
    PriceChangedSignal price_changed_;
public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{
	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

    boost::signals2::connection connect(PriceChangedSlot slot)
    {
        return price_changed_.connect(slot);
    }

	void set_price(double price)
	{
        if (price != price_)
        {
            price_ = price;

            price_changed_(symbol_, price_);
        }
	}
};

class Investor
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update_portfolio(const std::string& symbol, double price)
	{
        std::cout << name_ << " notified about change: "
                  << symbol << " - " << price << std::endl;
	}
};

#endif /*STOCK_HPP_*/
