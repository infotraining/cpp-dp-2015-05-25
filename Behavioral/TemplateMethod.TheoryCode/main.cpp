#include "template_method.hpp"

using namespace std;

void client(AbstractClass& ac)
{
	ac.template_method();
}

int main()
{
	std::unique_ptr<AbstractClass> ac { new ConcreteClassA };
	client(*ac);

	std::cout << "\n\n";

	ac.reset(new ConcreteClassB());
	client(*ac);
};
