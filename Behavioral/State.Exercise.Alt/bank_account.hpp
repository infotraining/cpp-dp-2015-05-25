#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

struct AccountContext
{
    int id;
    double balance;

    AccountContext(int id, double balance = 0.0) : id{id}, balance{balance}
    {}
};

class AccountState
{
public:
    virtual void withdraw(double amount, AccountContext& context) const = 0;
    virtual void deposit(double amount, AccountContext& context) const = 0;
    virtual void pay_interest(AccountContext& context) const = 0;
    virtual std::string status() const = 0;
    virtual ~AccountState() {}
};

class AccountStateBase : public AccountState
{
protected:
    double interest_rate_;
public:
    AccountStateBase(double interest_rate) : interest_rate_(interest_rate) {}

    virtual void deposit(double amount, AccountContext &context) const
    {
        context.balance += amount;
    }

    virtual void pay_interest(AccountContext& context) const
    {
        double interest = context.balance * interest_rate_;
        context.balance += interest;
    }
};

class NormalState : public AccountStateBase
{
public:
    NormalState(double interest_rate) : AccountStateBase(interest_rate)
    {}

    void withdraw(double amount, AccountContext& context) const
    {
        context.balance -= amount;
    }

    std::string status() const
    {
        return "Normal";
    }
};

class OverdraftState : public AccountStateBase
{
public:
    OverdraftState(double interest_rate) : AccountStateBase(interest_rate)
    {}

    void withdraw(double amount, AccountContext& context) const
    {
        std::cout << "Brak srodkow na koncie #" << context.id << std::endl;
    }

    std::string status() const
    {
        return "Overdraft";
    }
};

class BankAccount
{
    AccountContext context_;
    const AccountState* state_;

    static NormalState NORMAL;
    static OverdraftState OVERDRAFT;
protected:
	void update_account_state()
	{
        if (context_.balance < 0)
            state_ = &OVERDRAFT;
		else
            state_ = &NORMAL;
	}
public:
    BankAccount(int id) : context_(id), state_(&NORMAL) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(amount, context_);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        state_->deposit(amount, context_);

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(context_);
	}

    std::string status() const
    {
        std::stringstream strm;
        strm << "BankAccount #" << context_.id
             << "; State: " << state_->status()
             << "; Balance: " << context_.balance;

        return strm.str();
    }

	double balance() const
	{
        return context_.balance;
	}

	int id() const
	{
        return context_.id;
	}
};

#endif
